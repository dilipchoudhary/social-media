import pdb

from django.db.models import Q
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Post, Comment, Notification


class PostSerializer(ModelSerializer):
    likes_count = serializers.SerializerMethodField('get_likes_count')

    class Meta:
        model = Post
        fields = ['id', 'user', 'caption', 'text', 'image', 'likes_count']
        extra_kwargs = {
            "user": {"read_only": True}
        }

    def validate(self, attrs):
        attrs.update({'user': self.context.get('request').user})
        return attrs

    def get_likes_count(self, obj):
        return obj.likes.count()


class PostPKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context.get('request').user
        return Post.objects.filter(Q(user=user) | Q(user__friends=user)).distinct()


class CommentSerializer(ModelSerializer):
    post = PostPKField()

    class Meta:
        model = Comment
        fields = ['id', 'user', 'post', 'text', 'reply']
        extra_kwargs = {
            "user": {"read_only": True}
        }

    def validate(self, attrs):
        attrs.update({'user': self.context.get('request').user})
        return attrs


class PostLikeSerializer(ModelSerializer):
    class Meta:
        model = Post
        fields = ['id', 'user', 'likes']
        extra_kwargs = {
            "user": {"read_only": True}
        }

    # update many2many field likes
    def update(self, instance, validated_data):
        user = self.context.get('request').user
        if user != instance.user:
            instance.likes.remove(user) if (user in instance.likes.all()) else instance.likes.add(user)
            return super(PostLikeSerializer, self).update(instance, validated_data)
        return instance


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ['id', 'actor', 'description', 'read', 'date']

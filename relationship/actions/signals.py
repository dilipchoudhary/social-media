import pdb

from django.db.models.signals import m2m_changed, post_save
from .models import Post, Comment, Notification
from friend_request.models import FriendRequest
from accounts.models import User
from django.contrib.contenttypes.models import ContentType


#
# def notify_likes_on_post_changed(sender, **kwargs):
#     print("signal working")
#     instance = kwargs.pop('instance', None)
#     pk_set = kwargs.pop('pk_set', None)
#     action = kwargs.pop('action', None)
#     if action == "post_add":
#         data = {
#             'to_user': instance.user,
#             'title': "Post liked",
#             'description': "post liked by {} and counts- {}".format(instance.user.username, instance.likes.count())
#         }
#         for pk in pk_set:
#             user = User.objects.get(id=pk)
#             data.update({"from_user": user})
#         Notification.objects.create(**data)
#
#
# m2m_changed.connect(notify_likes_on_post_changed, sender=Post.likes.through, weak=True)
#
#
# # notification for comments
# def notify_comment_on_post(sender, **kwargs):
#     pdb; pdb.set_trace()
#     instance = kwargs.pop('instance', None)
#     # content_type = ContentType.objects.get_for_model(instance)
#     data = {
#         'from_user': instance.user,
#         'to_user': instance.post.user,
#         'title': "Commented",
#         'description': "comment on post {} by {} ".format(instance.post.caption, instance.user.username)
#     }
#     print(data)
#     Notification.objects.create(**data)
#
#
# post_save.connect(receiver=notify_comment_on_post, sender=Comment, weak=True)


def create_notification(sender, instance, **kwargs):
    action = kwargs.pop('action', None)
    created = kwargs.pop('created', None)

    data = {
        "content_type": ContentType.objects.get_for_model(instance),
        "object_id": instance.id,
        "description": None,
        "actor": None
    }
    if isinstance(instance, FriendRequest):
        if created:
            data.update({"description": f'Friend Request from {instance.from_user}',
                         "actor": instance.to_user})
        elif instance.status == FriendRequest.CONFIRM:
            data.update({"description": f'{instance.to_user} accepted your friend request',
                         "actor": instance.from_user}
                        )

    if isinstance(instance, Post):
        if action == "post_add":
            data.update({"description": f'{instance.text[:20]} post liked by {instance.likes.last()}',
                         "actor": instance.user})

    if isinstance(instance, Comment):
        """
        checking @username in comment
        user_list = re.findall('@(\w+)', instance.text)  # if user list do this first
        for user in user_list:
            obj = User.objects.filter(username=user)
            {"description": f' -instance.user- mentioned you in a comment -instance.text[:10]- ', 'actor': obj}
            notification = Notification.objects.create(**data)    
        """

        if instance.reply:
            data.update({"description": f'{instance.user} replied to your comment on {instance.post.text[:20]}',
                         "actor": instance.reply.user})
        elif created:
            data.update({"description": f'{instance.user} commented on {instance.post.text[:20]}',
                         "actor": instance.post.user})

    if data["description"]:
        notification = Notification.objects.create(**data)
        instance.notification.add(notification)


m2m_changed.connect(receiver=create_notification, sender=Post.likes.through, weak=True)
post_save.connect(receiver=create_notification, sender=Comment, weak=True)
post_save.connect(receiver=create_notification, sender=FriendRequest, weak=True)

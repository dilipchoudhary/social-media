from django.core.validators import FileExtensionValidator
from django.db import models
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel, ActivatorModel
from django.utils.translation import gettext_lazy as _
from accounts.models import User

# Create your models here.
"""
model for POST & COMMENT'S
will use moviepy to resize before saving
"""

# will be using generic relationship for this as we can have
# tag for multiple classes here post and comment
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

# NOTIFY WHEN
"""
POST - CHANGE, 
FRIEND REQUEST, COMMENT - ADD , CHANGE
"""


# ADDITION = 1
# CHANGE = 2
# ACTION_FLAG_CHOICES = (
#     (ADDITION, _('Addition')),
#     (CHANGE, _('Change')),
# )

class Tag(models.Model):
    tag = models.CharField(max_length=20, null=True, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.tag


class Notification(TitleDescriptionModel):
    actor = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notifications", null=False, blank=True)
    date = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(_('Read'), default=False, blank=True)

    # Below the mandatory fields for generic relation
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.description


class Post(TimeStampedModel):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='posts', null=False)
    caption = models.CharField(_('Caption'), max_length=50, null=True, blank=True)
    text = models.TextField(_('post text'))
    image = models.ImageField(upload_to='post_image', null=True, blank=True)
    # video = models.FileField(upload_to='videos_uploaded', null=True, validators=[ FileExtensionValidator(
    # allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv', 'gif']) ] )
    likes = models.ManyToManyField(to=User, related_name='user_likes', blank=True)
    tag = GenericRelation(Tag, related_query_name='posts')
    notification = GenericRelation(Notification, related_query_name="post")

    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return f'{self.caption}-{self.user.username}'


class Comment(TimeStampedModel):
    user = models.ForeignKey(to=User, related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(to=Post, related_name='comments', on_delete=models.CASCADE)
    text = models.CharField(_('Comment Text'), max_length=200, null=False, blank=False)
    tag = GenericRelation(Tag, related_query_name='comments')
    reply = models.ForeignKey('self', related_name='replies', on_delete=models.CASCADE, null=True, blank=True)
    notification = GenericRelation(Notification, related_query_name="comment")

    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return f'{self.user.username}'



    # video = models.FileField(upload_to='videos_uploaded', null=True, validators=[ FileExtensionValidator(
    # allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv', 'gif']) ] )

# class Notification(TitleDescriptionModel):
#     from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="+", null=False, blank=True)
#     to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notification_received", null=False,
#                                 blank=False)
#     read = models.BooleanField(_('Read'), default=False, blank=True)
#     created = models.DateField(_('created time'), auto_now_add=True)
#
#     class Meta:
#         ordering = ['-created']
#
#     def __str__(self):
#         return 'note {} - {}'.format(self.from_user, self.to_user)

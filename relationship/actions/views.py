import pdb

from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Post, Comment, Tag, Notification
from .serializers import PostSerializer, CommentSerializer, PostLikeSerializer, \
    NotificationSerializer
from rest_framework.permissions import IsAuthenticated
from accounts.apis.permissions import IsOwner
from django.db.models import Q, Count
from rest_framework.generics import UpdateAPIView, ListAPIView
from rest_framework.viewsets import ModelViewSet, GenericViewSet


# Create your views here.


class PostModelViewSet(ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method in ["POST", "PUT"]:
            self.permission_classes = [IsAuthenticated, IsOwner]
        return super(PostModelViewSet, self).get_permissions()

    def get_queryset(self):
        queryset = super(PostModelViewSet, self).get_queryset()
        return queryset.filter(Q(user=self.request.user) | Q(user__friends=self.request.user)).distinct()


class CommentModelViewSet(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method in ["POST", "PUT", "PATCH"]:
            self.permission_classes = [IsAuthenticated, IsOwner]
        return super(CommentModelViewSet, self).get_permissions()


class PostLikeView(UpdateAPIView):
    serializer_class = PostLikeSerializer
    queryset = Post.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super(PostLikeView, self).get_queryset()
        return queryset.filter(Q(user=self.request.user) | Q(user__friends=self.request.user)).distinct()


class NotificationApiView(ListAPIView):
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = super(NotificationApiView, self).get_queryset()
        queryset = queryset.filter(actor=self.request.user)
        return queryset


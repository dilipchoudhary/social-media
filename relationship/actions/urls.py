from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import PostModelViewSet, CommentModelViewSet, PostLikeView, NotificationApiView

app_name = 'actions'

routers = DefaultRouter(trailing_slash=False)
routers.register('post', PostModelViewSet, basename='posts')
routers.register('comment', CommentModelViewSet, basename='comment')
urlpatterns = routers.urls
urlpatterns += [
    path('post/<int:pk>/likes', PostLikeView.as_view(), name='likes'),
    path('notification', NotificationApiView.as_view(), name='notification'),

]

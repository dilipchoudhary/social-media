from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import ChatRoomModelViewSet, MessageModelViewSet

app_name = 'conversation'

routers = DefaultRouter(trailing_slash=False)
routers.register('chatroom', ChatRoomModelViewSet, basename='chat-room')
routers.register('message', MessageModelViewSet, basename='message')

urlpatterns = routers.urls

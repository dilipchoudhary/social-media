import pdb

from rest_framework import serializers
from .models import ChatRoom, Message
from accounts.models import User


class UsersPKField(serializers.PrimaryKeyRelatedField):
    def get_queryset(self):
        user = self.context['request'].user
        queryset = user.friends.all()
        return queryset


class ChatRoomSerializer(serializers.ModelSerializer):
    users = UsersPKField(many=True)

    class Meta:
        model = ChatRoom
        fields = ['id', 'title', 'users', 'created_by']
        extra_kwargs = {
            'title': {'required': False}
        }
        read_only_fields = ['created_by']

    def validate(self, attrs):
        attrs.update(
            {'created_by': self.context.get('request').user}
        )
        return attrs

    def update(self, instance, validated_data):
        users = validated_data.pop('users', None)
        if users:
            instance.users.add(*users)
        return super(ChatRoomSerializer, self).update(instance, validated_data)


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = ['id', 'sender', 'chat_room', 'text', 'image', 'reply', 'seen_by']
        read_only_fields = ['seen_by']

    def validate(self, attrs):
        attrs.update({'sender': self.context.get('request').user})
        return attrs


from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel, TitleDescriptionModel, ActivatorModel
from accounts.models import User


# Create your models here.


class ChatRoom(TimeStampedModel, ActivatorModel):
    title = models.CharField(max_length=20)
    users = models.ManyToManyField(User, related_name="chat_rooms")
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)

    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return self.title


# class SharedImage(models.Model):
#     image = models.ImageField(upload_to='shared_images')


class Message(TimeStampedModel, ActivatorModel):
    sender = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', blank=True)
    chat_room = models.ForeignKey(ChatRoom, on_delete=models.PROTECT, related_name='messages')
    text = models.TextField(_('message'), blank=True)
    seen_by = models.ManyToManyField(User, related_name='seen_messages')
    image = models.ImageField(upload_to='shared_images', null=True, blank=True)
    reply = models.ForeignKey('self', on_delete=models.PROTECT, related_name='replies', null=True, blank=True)

    def __str__(self):
        return self.sender.username

    class Meta:
        ordering = ['-created']

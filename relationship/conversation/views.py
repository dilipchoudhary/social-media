import pdb

from django.db.models import Q
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .models import ChatRoom, Message
from .serializer import ChatRoomSerializer, MessageSerializer
from rest_framework.mixins import CreateModelMixin, ListModelMixin, DestroyModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.status import HTTP_200_OK

# Create your views here.


class ChatRoomModelViewSet(ModelViewSet):
    serializer_class = ChatRoomSerializer
    queryset = ChatRoom.objects.active()
    permission_classes = [IsAuthenticated]

    def perform_destroy(self, instance):
        instance.status = instance.INACTIVE_STATUS
        instance.save()

    """ action to get all messages of a chat room"""
    @action(detail=True, methods=['get'])
    def message(self, request, pk):
        chat_room = self.get_object()
        messages = chat_room.messages.all()
        serializer = MessageSerializer(messages, many=True)

        """ updating all un seen message as seen by user  """
        seen_message_id_list = self.request.user.seen_messages.all().values_list('id', flat=True)
        unseen_messages = messages.exclude(id__in=seen_message_id_list)

        for message in unseen_messages:
            if message.sender != self.request.user and self.request.user not in message.seen_by.all():
                message.seen_by.add(self.request.user)
        return Response(serializer.data)

    @action(detail=True, methods=['patch'], url_path='remove/user')
    def remove(self, request, pk):
        obj = self.get_object()
        users = request.data.get('users')
        obj.users.remove(users)
        return Response({'message': 'user removed successfully'})


    def get_queryset(self):
        return super(ChatRoomModelViewSet, self).get_queryset().filter(
            Q(created_by=self.request.user) | Q(users=self.request.user)
        ).distinct()


class MessageModelViewSet(CreateModelMixin, DestroyModelMixin,
                          GenericViewSet):  # removing  ListModelMixin as we get message by chat id
    serializer_class = MessageSerializer
    queryset = Message.objects.active()
    permission_classes = [IsAuthenticated]

    def perform_destroy(self, instance):
        instance.status = instance.INACTIVE_STATUS
        instance.save()

    def get_queryset(self):
        return super(MessageModelViewSet, self).get_queryset().filter(
            Q(chat_room__created_by=self.request.user) | Q(chat_room__users=self.request.user)
        ).distinct()

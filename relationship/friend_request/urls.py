from rest_framework import routers
from .views import FriendRequestModelViewSet

app_name = 'friend_request'
routers = routers.DefaultRouter(trailing_slash=False)
routers.register('', FriendRequestModelViewSet, basename='friend_request')

urlpatterns = routers.urls

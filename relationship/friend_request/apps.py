from django.apps import AppConfig


class FriendRequestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'friend_request'

    def ready(self):
        import friend_request.signals
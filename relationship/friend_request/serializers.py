import pdb

from rest_framework import serializers
from .models import FriendRequest
from accounts.apis.serializers import UserSerializer


class FriendRequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = FriendRequest
        fields = ['id', 'from_user', 'to_user', 'status']
        read_only_fields = ['from_user']

    def validate(self, attrs):
        if not self.instance:
            attrs.update({
                            'from_user': self.context['request'].user,
                        })
            return attrs
        elif self.instance.from_user == self.context['request'].user:
            raise serializers.ValidationError({'non field errors': 'Bad Request'})
        return attrs

    def update(self, instance, validated_data):
        user = self.context.get('request').user
        instance = super(FriendRequestSerializer, self).update(instance, validated_data)
        # if request status change to confirm add him to user friend list and delete in views perform create
        if instance.status == FriendRequest.CONFIRM:
            data = {"friends": [instance.from_user_id]}
            user_serializer = UserSerializer(user, data=data, context={'request': self.context.get('request')})
            user_serializer.is_valid(raise_exception=True)
            user_serializer.save()
        return instance

from django.db import models
from accounts.models import User
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.contrib.contenttypes.fields import GenericRelation
from actions.models import Notification
# Create your models here.


class FriendRequest(TimeStampedModel):
    DECLINE = 0
    CONFIRM = 1
    PENDING = 2
    STATUS_TYPE = (
        (DECLINE, _('Decline')),
        (CONFIRM, _('Confirm')),
        (PENDING, _('Pending'))
    )

    from_user = models.ForeignKey(User, related_name='request_send', on_delete=models.CASCADE, blank=True)
    to_user = models.ForeignKey(User, related_name='request_received', on_delete=models.CASCADE)
    status = models.IntegerField(choices=STATUS_TYPE, default=PENDING)
    notification = GenericRelation(Notification, related_query_name="friend_request")

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '{} -> {}'.format(self.from_user, self.to_user)

    @property
    def is_confirm(self):
        return self.status == self.CONFIRM

    @property
    def is_decline(self):
        return self.status == self.DECLINE

    @property
    def is_pending(self):
        return self.status == self.PENDING

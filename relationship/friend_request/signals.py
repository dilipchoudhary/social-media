import pdb
# from actions.models import Notification
from .models import FriendRequest
from django.db.models.signals import post_save


# def notify_friend_request(sender, instance, **kwargs):
#     data = {
#         'from_user': instance.from_user,
#         'to_user': instance.to_user,
#         'title': "Friend Request",
#     }
#     if instance.status == FriendRequest.CONFIRM:
#         data.update({'description': "Friend Request accepted by {} ".format(instance.to_user)})
#         Notification.objects.create(**data)
#     elif instance.status == FriendRequest.PENDING:
#         data.update({'description': "Friend Request by {} ".format(instance.from_user)})
#         Notification.objects.create(**data)
#     else:
#         pass
#
#
# post_save.connect(receiver=notify_friend_request, sender=FriendRequest, weak=True)
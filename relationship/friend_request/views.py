import pdb

from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import FriendRequest
from .serializers import FriendRequestSerializer
from rest_framework.permissions import IsAuthenticated
from accounts.apis.permissions import IsOwner
from django.db.models import Q


# Create your views here.


class FriendRequestModelViewSet(ModelViewSet):
    serializer_class = FriendRequestSerializer
    queryset = FriendRequest.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = super(FriendRequestModelViewSet, self).get_queryset()
        return queryset.filter(Q(from_user=self.request.user) | Q(to_user=self.request.user)).distinct()

    def perform_update(self, serializer):
        super(FriendRequestModelViewSet, self).perform_update(serializer)
        instance = self.get_object()
        # deleting request instance if get get confirmed or declined
        # instance.delete()

import pdb

from rest_framework.serializers import ModelSerializer
from accounts.models import User
from rest_framework import urls
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import password_validation


# from rest_framework.authentication


class UserSerializer(ModelSerializer):
    new_password = serializers.CharField(max_length=128, write_only=True, required=False)
    confirm_password = serializers.CharField(max_length=128, write_only=True, required=False)

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "email",
            "password",
            "confirm_password",
            "new_password",
            "dob",
            "gender",
            "city",
            "bio",
            "image",
            "friends",
        ]
        extra_kwargs = {
            "password": {"write_only": True, "required":False},
            "username": {"write_only": True, "required":False},
        }

    def validate_new_password(self, value):
        password_validation.validate_password(value, user=self.context.get('user'))
        return value

    def validate_password(self, value):
        password_validation.validate_password(value, user=self.context.get('user'))
        return value

    def create(self, validated_data):
        password = validated_data.get("password", None)
        confirm_password = validated_data.pop("confirm_password", None)
        if password and confirm_password and password != confirm_password:
            raise serializers.ValidationError(
                {"confirm_password": _("Password And Confirm Password Did Not Match")}
            )
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data.get("password"))
        user.save()
        return user

    def update(self, instance, validated_data):
        """
        checking for password change put request and
        adding override m2m field friends as we need to append friend with id  to friends and not overwrite
        """
        user = self.context.get('request').user
        password = validated_data.pop('password', None)  # " if password we will update instance password"
        new_password = validated_data.pop('new_password', None)
        confirm_password = validated_data.pop('confirm_password', None)  # confirm new password
        if password and user.check_password(password):   # check user password user
            if new_password and new_password == confirm_password:
                instance.set_password(new_password)
                instance.save()
            else:
                raise serializers.ValidationError(
                    {"field error": _("Password and Confirm New Password did not match")}
                )
        # else:
        #     raise serializers.ValidationError(
        #         {"field error": _("Wrong User Password")}
        #     )
        friends = validated_data.pop("friends", None)

        if friends:
            instance.friends.add(*friends)
            # instance.save()
        return super(UserSerializer, self).update(instance, validated_data)

from rest_framework.generics import CreateAPIView
from rest_framework.viewsets import ModelViewSet
from .serializers import UserSerializer
from accounts.models import User
from rest_framework.permissions import AllowAny, IsAuthenticated
from .permissions import IsOwner
from django_filters.rest_framework import DjangoFilterBackend

class UserViewSet(ModelViewSet):
    """
    # User CRUD endpoints #
    ** Endpoint to Create, Retrieve, Update and Destroy  user instance. **

    ** POST (Create): api/accounts **
        {
        "username": "ajay123",
        "email": "ajay@gmail.com",
        "password": "#include1",
        "confirm_password": "#include1"
        }

    ** GET (Retrieve): /api/accounts/1 **
        {
            "id": 116,
            "username": "ajay123",
            "email": "ajay@gmail.com",
            "dob": null,
            "gender": null,
            "city": null,
            "bio": null,
            "friends": []
        }
    ** PUT (Full Update): api/accounts/1 **
        {
            "dob": '1993-06-18',
            "city": "gandhi nagar",
            "gender": "M",
            "bio": "a good listener",
            "friends": [2,4],
            "image":
        }

    ** PATCH (Partial Update): /api/accounts/1 **
        {
            "city": "surat",
        }
    ** PATCH (Password Change): /api/accounts/1 **
        {
            "password": "#include1"
            "new_password": "#changed1"
            "confirm_new_password": "#changed1"
        }
    ** DELETE (Destroy): /api/accounts/1 **

    # Field Legend #
        * username - "string"
        * password - "string"
        * email - "email" <- optional
        * first_name - "string"
        * last_name - "string"
        * dob - "date" <- date of birth - format (YYYY-MM-DD)
        * city - "string"
        * bio - "string"
        * gender - "string" <- optional
        * image - "image"
        * friends - self <- recursive manytomany relationship

        ** gender **

    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['username', 'city']

    def get_permissions(self):
        if self.request.method == 'POST':
            self.permission_classes = [AllowAny]
        elif self.request.method == 'GET':
            self.permission_classes = [IsAuthenticated]
        else:
            self.permission_classes = [IsAuthenticated, IsOwner]
            
        return super(UserViewSet, self).get_permissions()

        


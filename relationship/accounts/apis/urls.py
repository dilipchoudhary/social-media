from django.urls import path, include
from rest_framework import routers
from .views import UserViewSet
from rest_framework.authtoken import views as auth_views

app_name = 'apis'
routers = routers.DefaultRouter(trailing_slash=False)
routers.register('accounts', UserViewSet, basename='user')

urlpatterns = routers.urls
# urlpatterns += [path('auth/login', auth_views.obtain_auth_token, name='login')]

import datetime

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel, ActivatorModel


# Create your models here.

class User(AbstractUser):
    """
        Default User Class
    """
    FEMALE, MALE, OTHER = 'F', 'M', 'O'
    GENDER_TYPE = (
        (FEMALE, _('Female')),
        (MALE, _('Male')),
        (OTHER, _('Other'))
    )
    bio = models.TextField(_('About Your Self'), null=True, blank=True)
    dob = models.DateField(_('Date of birth '),  null=True, blank=True)
    image = models.ImageField(_('User Picture'), upload_to='profile_image', null=True, blank=True)
    friends = models.ManyToManyField('self', related_name='user_friends', blank=True)
    gender = models.CharField(_('Gender'), choices=GENDER_TYPE, null=True, blank=True, max_length=6)
    city = models.CharField(_('City'), max_length=50, null=True, blank=True)

    def __str__(self):
        return self.username

